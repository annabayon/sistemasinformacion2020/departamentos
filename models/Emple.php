<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emple".
 *
 * @property int $emp_no
 * @property string $apellido
 * @property string|null $oficio
 * @property int|null $dir
 * @property string|null $fecha_alt
 * @property int|null $salario
 * @property int|null $comision
 * @property int|null $dept_no
 *
 * @property Depart $deptNo
 */
class Emple extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emple';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_no', 'apellido', 'direccion'], 'required', 'message' => 'El campo es obligatorio'],
            [['emp_no', 'dir', 'salario', 'comision', 'dept_no'], 'integer'],
            [['fecha_alt'], 'safe'],
            [['apellido'], 'string', 'max' => 50],
            [['oficio'], 'string', 'max' => 30],
            [['emp_no'], 'unique'],
            [['dept_no'], 'exist', 'skipOnError' => true, 'targetClass' => Depart::className(), 'targetAttribute' => ['dept_no' => 'dept_no']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'emp_no' => 'Número de Empleado',
            'apellido' => 'Apellidos',
            'oficio' => 'Puesto',
            'dir' => 'Dirección',
            'fecha_alt' => 'Fecha Alta',
            'salario' => 'Salario Base',
            'comision' => 'Comisión',
            'dept_no' => 'Nombre de Departamento',
        ];
    }

    /**
     * Gets query for [[DeptNo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDeptNo()
    {
        return $this->hasOne(Depart::className(), ['dept_no' => 'dept_no']);
    }
}
